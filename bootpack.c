#include "bootpack.h"
#include "sprintf.h"

void HariMain(void)
{
  struct BOOTINFO *bootinfo = (struct BOOTINFO *) BOOTINFO_ADDR;
  struct Fifo8 timer_fifo, timer_fifo2, timer_fifo3;

  char s[40], mcursor[256], key_buffer[32], mouse_buffer[128],
       timer_buf[8], timer_buf2[8], timer_buf3[8];
  int mx, my, data;
  unsigned int memory_total;
  struct MouseDecoder mdec;
  struct MemoryManager *mm = (struct MemoryManager *) MM_ADDR;
  struct SheetController *sc;
  struct Sheet *sheet_bg, *sheet_mouse, *sheet_win;
  unsigned char *buf_bg, buf_mouse[256], *buf_win;
  struct Timer *timer, *timer2, *timer3;

  init_gdtidt();
  init_pic();
  io_sti();

  fifo8_init(&key_fifo, 32, key_buffer);
  fifo8_init(&mouse_fifo, 128, mouse_buffer);
  init_pit();
  io_out8(PIC0_IMR, 0xf8); // 1111 1001 PIC0でPIC1とキーボードを許可
  io_out8(PIC1_IMR, 0xef); // 1110 1111 PIC1でマウスを許可

  // Timers
  fifo8_init(&timer_fifo, 8, timer_buf);
  timer = timer_alloc();
  timer_init(timer, &timer_fifo, 1);
  timer_set_time(timer, 1000);
  fifo8_init(&timer_fifo2, 8, timer_buf2);
  timer2 = timer_alloc();
  timer_init(timer2, &timer_fifo2, 1);
  timer_set_time(timer2, 300);
  fifo8_init(&timer_fifo3, 8, timer_buf3);
  timer3 = timer_alloc();
  timer_init(timer3, &timer_fifo3, 1);
  timer_set_time(timer3, 50);


  init_keyboard();
  enable_mouse(&mdec);

  memory_total = memtest(0x00400000, 0xbfffffff);
  mm_init(mm);
  mm_free(mm, 0x00001000, 0x0009e000);
  mm_free(mm, 0x00400000, memory_total - 0x00400000);

  init_palette();

  sc = sc_init(mm, bootinfo->vram, bootinfo->screen_x, bootinfo->screen_y);

  sprintf(s, "sc->top: %d", sc->top);
  putfonts8_asc(bootinfo->vram, bootinfo->screen_x, 0, 0, COL8_FFFFFF, s);

  // Background
  sheet_bg = sc_alloc(sc);
  buf_bg = (unsigned char *) mm_alloc_4k(mm, bootinfo->screen_x * bootinfo->screen_y);
  sc_set_buf(sheet_bg, buf_bg, bootinfo->screen_x, bootinfo->screen_y, -1);
  init_screen(buf_bg, bootinfo->screen_x, bootinfo->screen_y);
  sc_slide(sheet_bg, 0, 0);
  sc_set_zindex(sheet_bg, 0);

  // Mouse
  sheet_mouse = sc_alloc(sc);
  sc_set_buf(sheet_mouse, buf_mouse, 16, 16, 99);
  init_mouse_cursor8(buf_mouse, 99);
  mx = (bootinfo->screen_x - 16) / 2;
  my = (bootinfo->screen_y - 28 - 16) / 2;
  sc_slide(sheet_mouse, mx, my);
  sc_set_zindex(sheet_mouse, 2);

  // Window
  sheet_win = sc_alloc(sc);
  buf_win = (unsigned char *) mm_alloc_4k(mm, 160 * 52);
  sc_set_buf(sheet_win, buf_win, 160, 52, -1);
  make_window8(buf_win, 160, 52, "counter");
  sc_slide(sheet_win, 80, 72);
  sc_set_zindex(sheet_win, 1);

  sprintf(s, "(%3d, %3d)", mx, my);
  putfonts8_asc(buf_bg, bootinfo->screen_x, 0, 0, COL8_FFFFFF, s);

  sprintf(s, "memory %dMB, free: %dKB",
      memory_total / (1024 * 1024), mm_total(mm) / 1024);
  putfonts8_asc(buf_bg, bootinfo->screen_x, 0, 32, COL8_FFFFFF, s);

  sc_refresh_sheet(sheet_bg, 0, 0, bootinfo->screen_x, 48);

  for (;;) {
    sprintf(s, "%010d", timer_ctl.count);
    boxfill8(buf_win, 160, COL8_C6C6C6, 40, 28, 119, 43);
    putfonts8_asc(buf_win, 160, 40, 28, COL8_000000, s);
    sc_refresh_sheet(sheet_win, 40, 28, 120, 44);

    io_cli();
    if (fifo8_status(&key_fifo) + fifo8_status(&mouse_fifo) + fifo8_status(&timer_fifo) + fifo8_status(&timer_fifo2) + fifo8_status(&timer_fifo3) == 0) {
      // io_stihlt();
      io_sti();
    } else {
      if (fifo8_status(&key_fifo) != 0) {
        data = fifo8_get(&key_fifo);
        io_sti();
        sprintf(s, "%02X", data);
        boxfill8(buf_bg, bootinfo->screen_x, COL8_008484, 0, 16, 15, 31);
        putfonts8_asc(buf_bg, bootinfo->screen_x, 0, 16, COL8_FFFFFF, s);
        sc_refresh_sheet(sheet_bg, 0, 16, 16, 32);
      } else if (fifo8_status(&mouse_fifo) != 0) {
        data = fifo8_get(&mouse_fifo);
        io_sti();
        if (decode_mouse(&mdec, data) != 0) {
          sprintf(s, "[lcr %4d %4d]", mdec.x, mdec.y);
          if ((mdec.btn & 0x01) != 0) s[1] = 'L';
          if ((mdec.btn & 0x02) != 0) s[3] = 'R';
          if ((mdec.btn & 0x04) != 0) s[2] = 'C';
          boxfill8(buf_bg, bootinfo->screen_x, COL8_008484, 32, 16, 32 + 15 * 8 - 1, 31);
          putfonts8_asc(buf_bg, bootinfo->screen_x, 32, 16, COL8_FFFFFF, s);
          sc_refresh_sheet(sheet_bg, 32, 16, 32 + 15 * 8, 32);

          // Move mouse cursor
          mx += mdec.x;
          my += mdec.y;
          if (mx < 0) mx = 0;
          if (my < 0) my = 0;
          if (mx > bootinfo->screen_x - 1) mx = bootinfo->screen_x - 1;
          if (my > bootinfo->screen_y - 1) my = bootinfo->screen_y - 1;
          sprintf(s, "(%3d, %3d)", mx, my);
          boxfill8(buf_bg, bootinfo->screen_x, COL8_008484, 0, 0, 79, 15);
          putfonts8_asc(buf_bg, bootinfo->screen_x, 0, 0, COL8_FFFFFF, s);
          sc_refresh_sheet(sheet_bg, 0, 0, 80, 16);
          sc_slide(sheet_mouse, mx, my);
        }
      } else if (fifo8_status(&timer_fifo) != 0) {
        data = fifo8_get(&timer_fifo);
        io_sti();
        putfonts8_asc(buf_bg, bootinfo->screen_x, 0, 64, COL8_FFFFFF, "10[sec]");
        sc_refresh_sheet(sheet_bg, 0, 64, 56, 80);
      } else if (fifo8_status(&timer_fifo2) != 0) {
        data = fifo8_get(&timer_fifo2);
        io_sti();
        putfonts8_asc(buf_bg, bootinfo->screen_x, 0, 80, COL8_FFFFFF, "3[sec]");
        sc_refresh_sheet(sheet_bg, 0, 80, 48, 96);
      } else if (fifo8_status(&timer_fifo3) != 0) {
        data = fifo8_get(&timer_fifo3);
        io_sti();
        if (data != 0) {
          timer_init(timer3, &timer_fifo3, 0);
          boxfill8(buf_bg, bootinfo->screen_x, COL8_FFFFFF, 8, 96, 15, 111);
        } else {
          timer_init(timer3, &timer_fifo3, 1);
          boxfill8(buf_bg, bootinfo->screen_x, COL8_008484, 8, 96, 15, 111);
        }
        timer_set_time(timer3, 50);
        sc_refresh_sheet(sheet_bg, 8, 96, 16, 112);
      }
    }
  }
}

void make_window8(unsigned char *buf, int xsize, int ysize, char *title) {
  static char closebtn[14][16] = {
    "OOOOOOOOOOOOOOO@",
		"OQQQQQQQQQQQQQ$@",
		"OQQQQQQQQQQQQQ$@",
		"OQQQ@@QQQQ@@QQ$@",
		"OQQQQ@@QQ@@QQQ$@",
		"OQQQQQ@@@@QQQQ$@",
		"OQQQQQQ@@QQQQQ$@",
		"OQQQQQ@@@@QQQQ$@",
		"OQQQQ@@QQ@@QQQ$@",
		"OQQQ@@QQQQ@@QQ$@",
		"OQQQQQQQQQQQQQ$@",
		"OQQQQQQQQQQQQQ$@",
		"O$$$$$$$$$$$$$$@",
		"@@@@@@@@@@@@@@@@"
  };

  int x, y;
  char c;
  boxfill8(buf, xsize, COL8_C6C6C6, 0,                 0, xsize - 1, 0        );
  boxfill8(buf, xsize, COL8_FFFFFF, 1,                 1, xsize - 2, 1        );
  boxfill8(buf, xsize, COL8_C6C6C6, 0,                 0,         0, ysize - 1);
  boxfill8(buf, xsize, COL8_FFFFFF, 1,                 1,         1, ysize - 2);
  boxfill8(buf, xsize, COL8_848484, xsize - 2,         1, xsize - 2, ysize - 2);
  boxfill8(buf, xsize, COL8_000000, xsize - 1,         0, xsize - 1, ysize - 1);
  boxfill8(buf, xsize, COL8_C6C6C6, 2,                 2, xsize - 3, ysize - 3);
  boxfill8(buf, xsize, COL8_000084, 3,                 3, xsize - 4, 20       );
  boxfill8(buf, xsize, COL8_848484, 1,         ysize - 2, xsize - 2, ysize - 2);
  boxfill8(buf, xsize, COL8_000000, 0,         ysize - 1, xsize - 1, ysize - 1);
  putfonts8_asc(buf, xsize, 24, 4, COL8_FFFFFF, title);
  for (y = 0; y < 14; y++) {
    for (x = 0; x < 16; x++) {
      c = closebtn[y][x];
      if (c == '@') c = COL8_000000;
      else if (c == '$') c = COL8_848484;
      else if (c == 'Q') c = COL8_C6C6C6;
      else c = COL8_FFFFFF;
      buf[(5 + y) * xsize + (xsize - 21 + x)] = c;
    }
  }
  return;
}
