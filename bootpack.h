// head.asm

#define BOOTINFO_ADDR 0x0ff0;

struct BOOTINFO {
  char cyls, leds, vmode, reserve;
  short screen_x, screen_y;
  char *vram;
};


// func.asm

void io_hlt(void);
void io_cli(void);
void io_sti(void);
void io_out8(int port, int data);
int io_load_eflags(void);
void io_store_eflags(int eflags);
void load_gdtr(int limit, int addr);
void load_idtr(int limit, int addr);
int load_cr0(void);
void store_cr0(int cr0);

void asm_inthandler20(void);
void asm_inthandler21(void);
void asm_inthandler27(void);
void asm_inthandler2c(void);

unsigned int memtest_sub(unsigned int start, unsigned int end);


// descriptor_table.c

#define GDT_ADDR  0x00270000
#define GDT_LIMIT 0x0000ffff

#define IDT_ADDR  0x0026f800
#define IDT_LIMIT 0x000007ff

#define BOOTPACK_ADDR  0x00280000
#define BOOTPACK_LIMIT 0x007ffff

#define AR_DATA32_RW 0x4092
#define AR_CODE32_ER 0x409a
#define AR_INTGATE32 0x008e

struct SegmentDescriptor {
  short limit_low, base_low;
  char base_mid, access_right;
  char limit_high, base_high;
};

struct GateDescriptor {
  short offset_low, selector;
  char dw_count, access_right;
  short offset_high;
};

void init_gdtidt(void);
void set_segmdesc(struct SegmentDescriptor *sd, unsigned int limit, int base, int ar);
void set_gatedesc(struct GateDescriptor *gd, int offset, int selector, int ar);



// fifo.c

struct Fifo8 {
  unsigned char *buf;
  int p, q, size, free, flags;
};
void fifo8_init(struct Fifo8 *fifo, int size, unsigned char *buf);
int fifo8_put(struct Fifo8 *fifo, unsigned char data);
int fifo8_get(struct Fifo8 *fifo);
int fifo8_status(struct Fifo8 *fifo);



// graphic.c

#define COL8_000000		0
#define COL8_FF0000		1
#define COL8_00FF00		2
#define COL8_FFFF00		3
#define COL8_0000FF		4
#define COL8_FF00FF		5
#define COL8_00FFFF		6
#define COL8_FFFFFF		7
#define COL8_C6C6C6		8
#define COL8_840000		9
#define COL8_008400		10
#define COL8_848400		11
#define COL8_000084		12
#define COL8_840084		13
#define COL8_008484		14
#define COL8_848484		15

void init_palette(void);
void set_palette(int start, int end, unsigned char *rgb);
void boxfill8(unsigned char *vram, int xsize, unsigned char c, int x0, int y0, int x1, int y1);
void init_screen(char *vram, int xsize, int ysize);
void putfont8(char *vram, int xsize, int x, int y, char c, char *font);
void putfonts8_asc(char *vram, int xsize, int x, int y, char c, unsigned char *s);
void init_mouse_cursor8(char *mouse, char bc);
void putblock8_8(char *vram, int vxsize, int pxsize,
    int pysize, int px0, int py0, char *buf, int bxsize);



// interruption.c

#define PIC0_ICW1 0x0020
#define PIC0_OCW2 0x0020
#define PIC0_IMR  0x0021
#define PIC0_ICW2 0x0021
#define PIC0_ICW3 0x0021
#define PIC0_ICW4 0x0021
#define PIC1_ICW1 0x00a0
#define PIC1_OCW2 0x00a0
#define PIC1_IMR  0x00a1
#define PIC1_ICW2 0x00a1
#define PIC1_ICW3 0x00a1
#define PIC1_ICW4 0x00a1

void init_pic(void);
void interruption_handler_20(int *esp);
void interruption_handler_21(int *esp);
void interruption_handler_27(int *esp);
void interruption_handler_2c(int *esp);



// keyboard.c

#define PORT_KEYDAT 0x0060
#define PORT_KEYCMD 0x0064

extern struct Fifo8 key_fifo;

void wait_KBC_sendready(void);
void init_keyboard(void);



// memory.c

#define MM_ADDR 0x003c0000
#define MM_MAX_BLOCKS 4090

struct MemoryFreeInfo {
  unsigned int addr, size;
};

struct MemoryManager {
  int num_free, max_free, lost_size, lost_count;
  struct MemoryFreeInfo free[MM_MAX_BLOCKS];
};

unsigned int memtest(unsigned int start, unsigned int end);
void mm_init(struct MemoryManager *mm);
unsigned int mm_total(struct MemoryManager *mm);
unsigned int mm_alloc(struct MemoryManager *mm, unsigned int size);
unsigned int mm_alloc_4k(struct MemoryManager *mm, unsigned int size);
int mm_free(struct MemoryManager *mm, unsigned int addr, unsigned int size);
int mm_free_4k(struct MemoryManager *mm, unsigned int addr, unsigned int size);



// mouse.c

struct MouseDecoder {
  unsigned char buf[3], phase;
  int x, y, btn;
};

extern struct Fifo8 mouse_fifo;

void enable_mouse(struct MouseDecoder *mdec);
int decode_mouse(struct MouseDecoder *mdec, unsigned char data);



// sheet.c

#define MAX_SHEETS 256

struct Sheet {
  unsigned char *buf;
  int width, height, x, y, tcolor, zindex, flags; // tcolor: transparent color
  struct SheetController *sc;
};

struct SheetController {
  unsigned char *vram, *map;
  int width, height, top;
  struct Sheet *sheets[MAX_SHEETS];
  struct Sheet sheets0[MAX_SHEETS];
};

struct SheetController *sc_init(struct MemoryManager *mm, unsigned char *vram, int width, int height);
struct Sheet *sc_alloc(struct SheetController *sc);
void sc_refresh(struct SheetController *sc);
void sc_set_buf(struct Sheet *s, unsigned char *buf, int width, int height, int tcolor);
void sc_set_zindex(struct Sheet *s, int zindex);
void sc_slide(struct Sheet *s, int x, int y);



// timer.c

#define MAX_TIMER 500

struct Timer {
  unsigned int timeout, flags;
  struct Fifo8 *fifo;
  unsigned char data;
};

struct TimerController {
  unsigned int count, next, using;
  struct Timer *timers[MAX_TIMER];
  struct Timer timers0[MAX_TIMER];
};

extern struct TimerController timer_ctl;
