# frozen_string_literal: true

font_file = ARGV[0]

index = nil

puts "char fonts[4096] = {"

open(font_file).each_line do |line|
  line = line.chomp

  if line == ""
    puts unless index.nil?
    index = nil
    next
  end

  if /^char 0x(?<hidx>..)$/ =~ line
    index = hidx
    print "    "
    next
  end

  next if index.nil?

  b = line.tr(".", "0").tr("*", "1")
  print "0x%02x," % b.to_i(2)
end

puts
puts "};"
