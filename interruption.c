#include "bootpack.h"
#include "sprintf.h"

void init_pic(void)
{
  io_out8(PIC0_IMR, 0xff);    // PIC0のすべてのIRQを無視
  io_out8(PIC1_IMR, 0xff);    // PIC1のすべてのIRQを無視

  io_out8(PIC0_ICW1, 0x11);   //
  io_out8(PIC0_ICW2, 0x20);   // IRQ0-7を `INT 0x20-0x27` と対応させる
  io_out8(PIC0_ICW3, 1 << 2); // PIC1をIRQ2で接続
  io_out8(PIC0_ICW4, 0x01);   //

  io_out8(PIC1_ICW1, 0x11);   //
  io_out8(PIC1_ICW2, 0x28);   // IRQ8-15を `INT 0x28-0x2f` と対応させる
  io_out8(PIC1_ICW3, 2);      // PIC1をIRQ2で接続
  io_out8(PIC1_ICW4, 0x01);   //

  io_out8(PIC0_IMR, 0xfb);    // 1111 1011 PIC0はIRQ2(PIC1)以外は無視
  io_out8(PIC1_IMR, 0xff);    // 1111 1111 PIC1のすべてのIRQを無視

  return;
}

#define PORT_KEYDAT 0x0060

struct Fifo8 key_fifo;
struct Fifo8 mouse_fifo;

// Handler for interruption from PS/2 keyboard
void interruption_handler_21(int *esp)
{
  unsigned char data;
  io_out8(PIC0_OCW2, 0x61);
  data = io_in8(PORT_KEYDAT);
  fifo8_put(&key_fifo, data);
  return;
}

// PIC0からの不完全割り込み対策
void interruption_handler_27(int *esp)
{
  io_out8(PIC0_OCW2, 0x67); // IRQ07 の受付完了をPICに通知
  return;
}

// Handler for interruption from PS/2 mouse
void interruption_handler_2c(int *esp)
{
  unsigned char data;
  io_out8(PIC1_OCW2, 0x64);
  io_out8(PIC0_OCW2, 0x62);
  data = io_in8(PORT_KEYDAT);
  fifo8_put(&mouse_fifo, data);
  return;
}
