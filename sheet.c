#include "bootpack.h"

#define SHEET_USE 1

struct SheetController *sc_init(struct MemoryManager *mm, unsigned char *vram, int width, int height) {
  struct SheetController *sc;
  int i;
  sc = (struct SheetController *) mm_alloc_4k(mm, sizeof(struct SheetController));
  if (sc == 0) {
    goto err;
  }
  sc->map = (unsigned char *) mm_alloc_4k(mm, width * height);
  if (sc->map == 0) {
    mm_free_4k(mm, (int) sc, sizeof (struct SheetController));
    goto err;
  }
  sc->vram = vram;
  sc->width = width;
  sc->height = height;
  sc->top = -1;
  for (i = 0; i < MAX_SHEETS; i++) {
    sc->sheets0[i].flags = 0;
    sc->sheets0[i].sc = sc;
  }
err:
  return sc;
}

struct Sheet *sc_alloc(struct SheetController *sc) {
  struct Sheet *s;
  int i;
  for (i = 0; i < MAX_SHEETS; i++) {
    if (sc->sheets0[i].flags == 0) {
      s = &sc->sheets0[i];
      s->flags = SHEET_USE;
      s->zindex = -1;
      return s;
    }
  }
  return 0;
}


void sc_set_buf(struct Sheet *s, unsigned char *buf, int width, int height, int tcolor) {
  s->buf = buf;
  s->width = width;
  s->height = height;
  s->tcolor = tcolor;
  return;
}

void sc_set_zindex(struct Sheet *s, int zindex) {
  struct SheetController *sc = s->sc;
  int i, old;
  old = s->zindex;

  if (zindex > sc->top + 1) {
    zindex = sc->top + 1;
  }
  if (zindex < -1) {
    zindex = -1;
  }
  s->zindex = zindex;

  if (old > zindex) { // Down
    if (zindex >= 0) {
      for (i = old; i > zindex; i--) {
        sc->sheets[i] = sc->sheets[i - 1];
        sc->sheets[i]->zindex = i;
      }
      sc->sheets[zindex] = s;
      sc_refresh_map(sc, s->x, s->y, s->x + s->width, s->y + s->height, zindex + 1);
      sc_refresh_sub(sc, s->x, s->y, s->x + s->width, s->y + s->height, zindex + 1, old);
    } else { // Hide shown sheet
      if (sc->top > old) {
        for (i = old; i < sc->top; i++) {
          sc->sheets[i] = sc->sheets[i + 1];
          sc->sheets[i]->zindex = i;
        }
      }
      sc->top--;
      sc_refresh_map(sc, s->x, s->y, s->x + s->width, s->y + s->height, 0);
      sc_refresh_sub(sc, s->x, s->y, s->x + s->width, s->y + s->height, 0, old - 1);
    }
  } else if (old < zindex) { // Up
    if (old >= 0) {
      for (i = old; i < zindex; i++) {
        sc->sheets[i] = sc->sheets[i + 1];
        sc->sheets[i]->zindex = i;
      }
      sc->sheets[zindex] = s;
    } else { // Show hidden sheet
      for (i = sc->top; i >= zindex; i--) {
        sc->sheets[i + 1] = sc->sheets[i];
        sc->sheets[i + 1]->zindex = i + 1;
      }
      sc->sheets[zindex] = s;
      sc->top++;
    }
    sc_refresh_map(sc, s->x, s->y, s->x + s->width, s->y + s->height, zindex);
    sc_refresh_sub(sc, s->x, s->y, s->x + s->width, s->y + s->height, zindex, zindex);
  }
  return;
}

void sc_refresh(struct SheetController *sc) {
  int i, x, y, vx, vy;
  unsigned char *buf, c, *vram;
  struct Sheet *s;
  vram = sc->vram;

  for (i = 0; i <= sc->top; i++) {
    s = sc->sheets[i];
    buf = s->buf;
    for (y = 0; y < s->height; y++) {
      vy = s->y + y;
      for (x = 0; x < s->width; x++) {
        vx = s->x + x;
        c = buf[y * s->width + x];
        if (c != s->tcolor) vram[vy * sc->width + vx] = c;
      }
    }
  }
  return;
}

void sc_refresh_sub(struct SheetController *sc, int x0, int y0, int x1, int y1, int zindex0, int zindex1) {
  int i, x, y, vx, vy, bx0, by0, bx1, by1;
  unsigned char *buf, c, *vram, *map, sid;
  struct Sheet *s;
  vram = sc->vram;
  map = sc->map;

  if (x0 < 0) x0 = 0;
  if (y0 < 0) y0 = 0;
  if (x1 > sc->width) x1 = sc->width;
  if (y1 > sc->height) y1 = sc->height;

  for (i = zindex0; i <= zindex1; i++) {
    s = sc->sheets[i];
    buf = s->buf;
    sid = s - sc->sheets0;

    bx0 = x0 - s->x;
    by0 = y0 - s->y;
    bx1 = x1 - s->x;
    by1 = y1 - s->y;
    if (bx0 < 0) bx0 = 0;
    if (by0 < 0) by0 = 0;
    if (bx1 > s->width) bx1 = s->width;
    if (by1 > s->height) by1 = s->height;

    for (y = by0; y < by1; y++) {
      vy = s->y + y;
      for (x = bx0; x < bx1; x++) {
        vx = s->x + x;
        if (map[vy * sc->width + vx] == sid)
          vram[vy * sc->width + vx] = buf[y * s->width + x];
      }
    }
  }
  return;
}

void sc_refresh_sheet(struct Sheet *s, int x0, int y0, int x1, int y1) {
  if (s->zindex >= 0)
    sc_refresh_sub(s->sc, s->x + x0, s->y + y0, s->x + x1, s->y + y1, s->zindex, s->zindex);
  return;
}

void sc_slide(struct Sheet *s, int x, int y) {
  int old_x = s->x, old_y = s->y;
  s->x = x;
  s->y = y;
  if (s->zindex >= 0) {
    sc_refresh_map(s->sc, old_x, old_y, old_x + s->width, old_y + s->height, 0);
    sc_refresh_map(s->sc, x, y, x + s->width, y + s->height, s->zindex);
    sc_refresh_sub(s->sc, old_x, old_y, old_x + s->width, old_y + s->height, 0, s->zindex - 1);
    sc_refresh_sub(s->sc, x, y, x + s->width, y + s->height, s->zindex, s->zindex);
  }
  return;
}

void sc_free(struct Sheet *s) {
  if (s->zindex >= 0) {
    sc_set_zindex(s, -1);
  }
  s->flags = 0;
  return;
}

void sc_refresh_map(struct SheetController *sc, int vx0, int vy0, int vx1, int vy1, int zindex0) {
  int i, bx, by, vx, vy, bx0, by0, bx1, by1;
  unsigned char *buf, sid, *map = sc->map;
  struct Sheet *s;

  if (vx0 < 0) vx0 = 0;
  if (vy0 < 0) vy0 = 0;
  if (vx1 > sc->width) vx1 = sc->width;
  if (vy1 > sc->height) vy1 = sc->height;

  for (i = zindex0; i <= sc->top; i++) {
    s = sc->sheets[i];
    sid = s - sc->sheets0;
    buf = s->buf;
    bx0 = vx0 - s->x;
    by0 = vy0 - s->y;
    bx1 = vx1 - s->x;
    by1 = vy1 - s->y;
    if (bx0 < 0) bx0 = 0;
    if (by0 < 0) by0 = 0;
    if (bx1 > s->width) bx1 = s->width;
    if (by1 > s->height) by1 = s->height;

    for (by = by0; by < by1; by++) {
      vy = s->y + by;
      for (bx = bx0; bx < bx1; bx++) {
        vx = s->x + bx;
        if (buf[by * s->width + bx] != s->tcolor) map[vy * sc->width + vx] = sid;
      }
    }
  }
  return;
}
