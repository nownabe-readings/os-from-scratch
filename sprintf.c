#include <stdarg.h>

int sprintf_d(char *buff, int val, int zeroflag, int width) {
  int tmp[10];
  int i, len = 0, is_minus = 0;

  if (val < 0) {
    is_minus = 1;
    val = ~val;
    val++;
  }

  while(1) {
    tmp[len++] = val % 10;
    if (val < 10) break;
    val /= 10;
  }

  i = len;

  // Padding
  if (zeroflag) {
    if (is_minus) {
      *(buff++) = '-';
      len++;
    }
    while (width - len > 0) {
      *(buff++) = '0';
      len++;
    }
  } else {
    while (width - len - is_minus > 0) {
      *(buff++) = ' ';
      len++;
    }
    if (is_minus) {
      *(buff++) = '-';
      len++;
    }
  }

  // Copy string
  while(i) {
    *(buff++) = (char)(tmp[--i]) + '0';
  }

  return(len);
}

int sprintf_x(char *buff, int rval, int zeroflag, int width, int is_capital) {
  int tmp[10];
  int i, len = 0;
  char base;
  unsigned int val = rval;

  while(1) {
    tmp[len++] = val % 16;
    if (val < 16) break;
    val >>= 4;
  }

  i = len;

  // Padding
  while (width - len > 0) {
    *(buff++) = zeroflag ? '0' : ' ';
    len++;
  }

  base = is_capital ? 'A' : 'a';

  // Copy
  while(i) {
    i--;
    *(buff++) = tmp[i] < 10 ? '0' + tmp[i] : base + tmp[i] - 10;
  }

  return(len);
}

int sprintf(char* buff, char* fmt, ...) {
	int len, size, zeroflag, width;
  va_list arg;
  va_start(arg, fmt);

	len = 0;

	while(*fmt){
		if(*fmt == '%'){
			zeroflag = width = 0;
			fmt++;

			if (*fmt == '0'){
				zeroflag = 1;
				fmt++;
			}

			while ((*fmt >= '0') && (*fmt <= '9')) {
        if (width != 0) width *= 10;
				width += *fmt - '0';
        fmt++;
			}

			switch(*fmt){
			case 'd':
				size = sprintf_d(buff, va_arg(arg, int), zeroflag, width);
				break;
			case 'x':
				size = sprintf_x(buff, va_arg(arg, int), zeroflag, width, 0);
				break;
			case 'X':
				size = sprintf_x(buff, va_arg(arg, int), zeroflag, width, 1);
				break;
			case 'c':
				// size = sprintf_c(buff, va_arg(arg, int));
				break;
			case 's':
				// size = sprintf_s(buff, va_arg(arg, char*));
				break;
			default:
        *buff = *fmt;
        size = 1;
				break;
			}
		} else {
			*buff = *fmt;
      size = 1;
		}

    len += size;
    buff += size;
    fmt++;
	}

	*buff = '\0';

	va_end(arg);
	return (len);
}
