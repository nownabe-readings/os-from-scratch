ORG 0x7c00

; for floppy disk

JMP entry
DB  0x90
DB	"HELLOIPL"
DW	512
DB	1
DW	1
DB	2
DW	224
DW	2880
DB	0xf0
DW	9
DW	18
DW	2
DD	0
DD	2880
DB	0,0,0x29
DD	0xffffffff
DB	"HELLO-OS   "
DB	"FAT12   "
RESB	18

;

entry:
  MOV ax, 0
  MOV ss,ax
  MOV sp,0x7c00
  MOV ds,ax
  MOV es,ax
  MOV si,msg

putloop:
  mov al,[si]
  add si,1
  cmp al,0
  je  fin
  mov ah,0x0e         ; 1文字表示ファンクション
  mov bx,15           ; カラーコード
  int 0x10            ; ビデオBIOS呼び出し
  jmp putloop

fin:
  hlt
  jmp fin             ; 無限ループ

msg:
  db  0x0a,0x0a       ; 改行2つ
  db  "hello, world"
  db  0x0a
  db  0

	resb 0x1fe-($-$$)

	db	0x55, 0xaa

	db	0xf0, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00
	resb 4600
	db 0xf0, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00
	resb	1469432
