#include "bootpack.h"

void init_gdtidt(void)
{
  struct SegmentDescriptor *gdt = (struct SegmentDescriptor *) GDT_ADDR;
  struct GateDescriptor    *idt = (struct GateDescriptor    *) IDT_ADDR;
  int i;

  // Initilize GDT
  for (i = 0; i <= GDT_LIMIT / 8; i++) {
    set_segmdesc(gdt + i, 0, 0, 0);
  }
  set_segmdesc(gdt + 1, 0xffffffff, 0x00000000, AR_DATA32_RW);
  set_segmdesc(gdt + 2, BOOTPACK_LIMIT, BOOTPACK_ADDR, AR_CODE32_ER);
  load_gdtr(GDT_LIMIT, GDT_ADDR);

  // Initilize IDT
  for (i = 0; i <= IDT_LIMIT / 8; i++) {
    set_gatedesc(idt + i, 0, 0, 0);
  }
  load_idtr(IDT_LIMIT, IDT_ADDR);

  set_gatedesc(idt + 0x20, (int) asm_inthandler20, 2 << 3, AR_INTGATE32);
  set_gatedesc(idt + 0x21, (int) asm_inthandler21, 2 << 3, AR_INTGATE32);
  set_gatedesc(idt + 0x27, (int) asm_inthandler27, 2 << 3, AR_INTGATE32);
  set_gatedesc(idt + 0x2c, (int) asm_inthandler2c, 2 << 3, AR_INTGATE32);

  return;
}

void set_segmdesc(struct SegmentDescriptor *sd, unsigned int limit, int base, int ar)
{
  if (limit > 0xfffff) {
    ar |= 0x8000;
    limit /= 0x1000;
  }
  sd->limit_low    = limit & 0xffff;
  sd->base_low     = base & 0xffff;
  sd->base_mid     = (base >> 16) & 0xff;
  sd->access_right = ar & 0xff;
  sd->limit_high   = ((limit >> 16) & 0x0f) | ((ar >> 8) & 0xf0);
  sd->base_high    = (base >> 24) & 0xff;
  return;
}

void set_gatedesc(struct GateDescriptor *gd, int offset, int selector, int ar)
{
  gd->offset_low   = offset & 0xffff;
  gd->selector     = selector;
  gd->dw_count     = (ar >> 8) & 0xff;
  gd->access_right = ar & 0xff;
  gd->offset_high  = (offset >> 16) & 0xffff;
  return;
}
