OBJS = bootpack.o descriptor_table.o fifo.o fonts.o func.o graphic.o interruption.o keyboard.o memory.o mouse.o sheet.o sprintf.o timer.o

all: os.img

ipl.bin: ipl.asm
	nasm ipl.asm -o ipl.bin -l ipl.lst

head.bin: head.asm
	nasm -o head.bin -l head.lst head.asm

func.o: func.asm
	nasm -o func.o -f elf32 func.asm

# -c Compile and assemble, but do not link.
# -m32 32bit用のオブジェクトファイルを生成する
# -fno-pic 非PIC (位置独立コードにしない)
%.o: %.c
	gcc -w -c -m32 -fno-pic -nostdlib -o $*.o $*.c

bootpack.bin: bootpack.ld $(OBJS)
	ld -m elf_i386 -e HariMain -o bootpack.bin -T bootpack.ld $(OBJS)

os.sys: head.bin bootpack.bin
	cat head.bin bootpack.bin > os.sys

os.img: ipl.bin os.sys
	mformat -f 1440 -C -B ipl.bin -i os.img
	mcopy os.sys -i os.img ::

fonts.c: hankaku.txt
	ruby convert_fonts.rb $< > $@

# fonts.o: fonts.c
# 	gcc -c -m32 -fno-pic -o fonts.o fonts.c

.PHONY: run
run: os.img
	qemu-system-i386 -m 32M -fda os.img

.PHONY: clean
clean:
	rm -f *.bin *.lst *.img *.o *.sys fonts.c
