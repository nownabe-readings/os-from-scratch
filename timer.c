#include "bootpack.h"

#define TIMER_FLAGS_ALLOC 1
#define TIMER_FLAGS_USING 2

#define PIT_CTRL 0x0043
#define PIT_CNT0 0x0040

struct TimerController timer_ctl;

void init_pit(void) {
  int i, j;
  io_out8(PIT_CTRL, 0x34);
  io_out8(PIT_CNT0, 0x9c);
  io_out8(PIT_CNT0, 0x2e);
  timer_ctl.count = 0;
  timer_ctl.next = 0xffffffff;
  timer_ctl.using = 0;
  for (i = 0; i < MAX_TIMER; i++) {
    timer_ctl.timers0[i].flags = 0;
  }
  return;
}

void interruption_handler_20(int *esp) {
  int i, j;
  io_out8(PIC0_OCW2, 0x60);
  timer_ctl.count++;
  if (timer_ctl.next > timer_ctl.count) return;

  for (i = 0; i < timer_ctl.using; i++) {
    if (timer_ctl.timers[i]->timeout > timer_ctl.count) break;
    timer_ctl.timers[i]->flags = TIMER_FLAGS_ALLOC;
    fifo8_put(timer_ctl.timers[i]->fifo, timer_ctl.timers[i]->data);
  }
  timer_ctl.using -= i;
  for (j = 0; j < timer_ctl.using; j++) {
    timer_ctl.timers[j] = timer_ctl.timers[i + j];
  }
  if (timer_ctl.using > 0) {
    timer_ctl.next = timer_ctl.timers[0]->timeout;
  } else {
    timer_ctl.next = 0xffffffff;
  }
  return;
}

struct Timer *timer_alloc(void) {
  int i;
  for (i = 0; i < MAX_TIMER; i++) {
    if (timer_ctl.timers0[i].flags == 0) {
      timer_ctl.timers0[i].flags = TIMER_FLAGS_ALLOC;
      return &timer_ctl.timers0[i];
    }
  }
  return 0;
}

void timer_free(struct Timer *timer) {
  timer->flags = 0;
  return;
}

void timer_init(struct Timer *timer, struct Fifo8 *fifo, unsigned char data) {
  timer->fifo = fifo;
  timer->data = data;
  return;
}

void timer_set_time(struct Timer * timer, unsigned int timeout) {
  int e, i, j;

  timer->timeout = timeout + timer_ctl.count;
  timer->flags = TIMER_FLAGS_USING;

  e = io_load_eflags();
  io_cli();

  for (i = 0; i < timer_ctl.using; i++) {
    if (timer_ctl.timers[i]->timeout >= timer->timeout) break;
  }

  for (j = timer_ctl.using; j > i; j--) {
    timer_ctl.timers[j] = timer_ctl.timers[j - 1];
  }

  timer_ctl.using++;
  timer_ctl.timers[i] = timer;
  timer_ctl.next = timer_ctl.timers[0]->timeout;

  io_store_eflags(e);
  return;
}
