#include "bootpack.h"

#define EFLAGS_AC_BIT 0x00040000
#define CR0_CACHE_DISABLE 0x60000000

unsigned int memtest(unsigned int start, unsigned int end) {
  char flg486 = 0;
  unsigned int eflg, cr0, i;

  // Check 386 or 486?
  eflg = io_load_eflags();
  eflg |= EFLAGS_AC_BIT;
  io_store_eflags(eflg);
  eflg = io_load_eflags();
  if ((eflg & EFLAGS_AC_BIT) != 0) flg486 = 1;

  eflg &= ~EFLAGS_AC_BIT;
  io_store_eflags(eflg);

  // Disable cache if 486.
  if (flg486 != 0) {
    cr0 = load_cr0();
    cr0 |= CR0_CACHE_DISABLE;
    store_cr0(cr0);
  }

  i = memtest_sub(start, end);

  // Enable cache if 486.
  if (flg486 != 0) {
    cr0 = load_cr0();
    cr0 &= ~CR0_CACHE_DISABLE;
    store_cr0(cr0);
  }

  return i;
}


void mm_init(struct MemoryManager *mm) {
  mm->num_free = 0;
  mm->max_free = 0;
  mm->lost_size = 0;
  mm->lost_count = 0;
}

/* Count the amount of free memory size */
unsigned int mm_total(struct MemoryManager *mm) {
  unsigned int i, t = 0;
  for (i = 0; i < mm->num_free; i++) t += mm->free[i].size;
  return t;
}

unsigned int mm_alloc(struct MemoryManager *mm, unsigned int size) {
  unsigned int i, addr;

  for (i = 0; i < mm->num_free; i++) {
    if (mm->free[i].size >= size) {
      addr = mm->free[i].addr;
      mm->free[i].addr += size;
      mm->free[i].size -= size;
      if (mm->free[i].size == 0) {
        mm->num_free--;
        for (; i < mm->num_free; i++) mm->free[i] = mm->free[i+1];
      }
      return addr;
    }
  }
  return 0;
}

unsigned int mm_alloc_4k(struct MemoryManager *mm, unsigned int size) {
  unsigned int a;
  size = (size + 0xfff) & 0xfffff000;
  a = mm_alloc(mm, size);
  return a;
}

int mm_free(struct MemoryManager *mm, unsigned int addr, unsigned int size) {
  int i, j;

  // Insert free block to be sorted by address.
  for (i = 0; i < mm->num_free; i++)
    if (mm->free[i].addr > addr) break;

  // Combine free blocks.
  if (i > 0 && mm->free[i-1].addr + mm->free[i-1].size == addr) {
    mm->free[i-1].size += size;
    if (i < mm->num_free && addr + size == mm->free[i].addr) {
      mm->free[i-1].size += mm->free[i].size;
      mm->num_free--;
      for (; i < mm->num_free; i++) mm->free[i] = mm->free[i+1];
    }
    return 0;
  }

  if (i < mm->num_free && addr + size == mm->free[i].addr) {
    mm->free[i].addr = addr;
    mm->free[i].size += size;
    return 0;
  }

  // Insert new block.
  if (mm->num_free < MM_MAX_BLOCKS) {
    for (j = mm->num_free; j > i; j--) mm->free[j] = mm->free[j-1];
    mm->num_free++;
    if (mm->max_free < mm->num_free) mm->max_free = mm->num_free;
    mm->free[i].addr = addr;
    mm->free[i].size = size;
    return 0;
  }

  mm->lost_count++;
  mm->lost_size += size;

  return -1;
}

int mm_free_4k(struct MemoryManager *mm, unsigned int addr, unsigned int size) {
  int i;
  size = (size + 0xfff) & 0xfffff000;
  i = mm_alloc(mm, size);
  return i;
}
